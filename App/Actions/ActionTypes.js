export const COUNTER_INCREMENT = 'counter_increment';
export const COUNTER_DECREMENT = 'counter_decrement';
export const SERVICE_PENDING = 'service_pending';
export const SERVICE_EMPTY = 'service_empty';

export const SERVICE_ERROR = 'service_error';
export const SERVICE_SUCCESS = 'service_success';
export const SET_DATE_TIME = 'set_date_time';
export const CHANGE_MONTH = 'change_month';


