import { connect } from 'react-redux';

import * as Actions from './ActionTypes';
import ConsumptionComponent from '../Components/ConsumptionComponent';
const moment = require('moment');
const mapStateToProps = (state) => ({
     isLoading: state.ConsumptionReducer.isLoading,
     error: state.ConsumptionReducer.error,
     data: state.ConsumptionReducer.data,
     kWhTotal: state.ConsumptionReducer.kWhTotal,
     krTotal: state.ConsumptionReducer.krTotal,
     errorMessage: state.ConsumptionReducer.errorMessage,
     activeTab:state.ConsumptionReducer.activeTab,
     currentDate:state.ConsumptionReducer.currentDate

});

const mapDispatchToProps = (dispatch) => ({
    callService: (state) => dispatch(callWebservice(state)),
    callActiveTab: (state) => { dispatch(callActiveTabService(state)) },
    callChangeMonth: (type,newDate) => { dispatch(callChangeMonthService(type,newDate)) }
    

});
export const callChangeMonthService = (type,newDate) => {
 return dispatch => {
    var currentDate = (type == "0") ? moment(newDate).subtract(1, 'M') : moment(newDate).add(1, 'M')
     dispatch(serviceChangeMonthSuccess(currentDate))
     dispatch(callWebservice(currentDate))
 }
}

export const callActiveTabService = (value) => {
 return dispatch => {
     dispatch(serviceActionTabSuccess(value))
 }
}

export const callWebservice = (currentDate) => {

        var currentDate =  currentDate; 
        var startDay = moment(currentDate).startOf('month').format("YYYY-MM-DD");

        var endDay = moment(currentDate).endOf('month').format("YYYY-MM-DD");
        var startTimeDiff = moment.duration("04:30:00");
        //startTimeDiff.set({hour:0,minute:0,second:0,millisecond:0})

        startDay = moment(startDay).add(startTimeDiff);
        endDay = moment(endDay).add(startTimeDiff);

        startDay = moment(startDay).toISOString();
        endDay = moment(endDay).add(1, "d").toISOString();

    return dispatch => {
        dispatch(serviceActionPending())
        var formBody = JSON.stringify({
            jsonrpc: "2.0",
            id: "1",
            method: "co.getbarry.megatron.controller.ConsumptionFreemiumController.getDailyConsumptionWithPrice",
            params: [startDay,endDay, "CET"],
        });
        console.log("===formBody",formBody);
        return fetch('https://jsonrpc.getbarry.dk/json-rpc', {
            method: 'POST',
            body: formBody
        }).then((response) => response.json())
            .then((responseJson) => {

                let kWhTotal = 0;
                let krTotal = 0;
                    responseJson.result.map((item, index) => {
                        kWhTotal += item.value;
                        krTotal += item.priceIncludingVat;
                    })
                console.log("==responseJsonasdasdfasddsf==", responseJson,kWhTotal,krTotal);
                dispatch(serviceActionSuccess(responseJson.result,kWhTotal,krTotal))
            }).catch((error) => {
                console.error(error);
                dispatch(serviceActionError(error))
            });
    }
}

export const serviceActionPending = () => ({
    type: Actions.SERVICE_PENDING
})

export const serviceActionError = (error) => ({
    type: Actions.SERVICE_ERROR,
    error: error
})

export const serviceActionEmptyData = () => ({
    type: Actions.SERVICE_EMPTY
})

export const serviceActionSuccess = (data,kWhTotal,krTotal) => ({
    type: Actions.SERVICE_SUCCESS,
    data: data,
    kWhTotal:kWhTotal,
    krTotal:krTotal

})

export const serviceActionTabSuccess = (value) => ({
    type: Actions.SET_DATE_TIME,
    activeTab:value
    
})

export const serviceChangeMonthSuccess = (value) => ({
    type: Actions.CHANGE_MONTH,
    currentDate:value
    
})

export default connect(mapStateToProps, mapDispatchToProps)(ConsumptionComponent);