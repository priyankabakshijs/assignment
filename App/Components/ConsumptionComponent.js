import React, { Component, Fragment } from 'react';
import {
    StyleSheet,
    Text,
    View,
    SafeAreaView,
    StatusBar,
    Dimensions,
    Image,
    TouchableOpacity,
    ActivityIndicator,
    FlatList, Alert,

} from 'react-native';
const { width, height } = Dimensions.get("window");
var moment = require('moment');


export default class Consumption extends Component {

    constructor(props) {
        super(props)
        this.state = {
             
        }

    }
    componentDidMount() {
        this.props.callService(new Date())
    }

    section1() {

        return (
            <View style={styles.section1}>

                <View style={styles.headerBox}>
                    <Text style={styles.headerText}> Your consumption </Text>
                </View>
                <View style={styles.monthPicker}>
                    <TouchableOpacity
                        onPress={() => this.props.callChangeMonth("0", this.props.currentDate)}
                        style={styles.arrows}>
                        <Image style={styles.monthIcon}
                            source={require('./../../assets/images/left.png')}
                            resizeMode="contain"></Image>
                    </TouchableOpacity>
                    <View style={styles.monthBox}>
                        <Text style={styles.headerText}>{moment(this.props.currentDate).format("MMMM YYYY")}</Text>
                    </View>
                    <TouchableOpacity
                        onPress={() => this.props.callChangeMonth("1", this.props.currentDate)}
                        style={styles.arrows}>
                        <Image style={styles.monthIcon} source={require('./../../assets/images/right.png')} resizeMode="contain"></Image>
                    </TouchableOpacity>

                </View>
                <View style={styles.tabStyle}>
                    <View style={styles.tabBlock}>
                        <TouchableOpacity
                            onPress={() => this.props.callActiveTab("0")}
                            style={[styles.tabButton, { backgroundColor: this.props.activeTab == "0" ? "#FF4B9B" : "#32363D" }]}>
                            <Text style={styles.tabText}>kWh</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.props.callActiveTab("1")}
                            style={[styles.tabButton, { backgroundColor: this.props.activeTab == "1" ? "#EF7B40" : "#32363D" }]}>
                            <Text style={styles.tabText}>kr. </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
    header() {
        return (
            <View style={styles.flatListHeader}>
                <Text style={[styles.fWhValue, { color: this.props.activeTab == "0" ? "#FF4B9B" : "#EF7B40", }]}>
                    {this.props.activeTab == "0" ? this.props.kWhTotal.toFixed(2) : this.props.krTotal.toFixed(2)}<Text style={styles.textType}> {this.props.activeTab == "0" ? "kWh" : "kr."}</Text></Text>
            </View>
        )
    }
    listEmpty = () => {
        return (
            <View style={styles.emptyBarView}>
                <Text style={styles.emptyText}>  {this.props.errorMessage}  </Text>
            </View>
        );
    }

    renderSeparator = () => {
        return (<View style={styles.itemBorder} />);
    }
    section2() {
        return (
            <View style={styles.section2}>
                <View style={styles.section2}>
                    <FlatList
                        keyExtractor={(item, index) => index.toString()}
                        data={this.props.data}
                        refreshing={this.props.isLoading}
                        renderItem={({ item }) =>
                            <View>
                                <TouchableOpacity style={styles.itemBox}
                                    onPress={this.getListViewItem.bind(this, item)}>
                                    <View style={{ borderWidth: 0 }}>
                                        <Text style={styles.itemValue}>{moment(item.date).format("DD/MM YYYY")}</Text>
                                    </View>
                                    <View style={styles.innerItemBox}>
                                        <Text style={styles.itemValue}>
                                            {this.props.activeTab == "0" ? item.value.toFixed(2) : item.priceIncludingVat.toFixed(2)}
                                        </Text>
                                        <Text style={styles.itemSign}>
                                            {this.props.activeTab == "0" ? "kWh" : "kr."}
                                        </Text>
                                        <View style={styles.imageCenterBox}>
                                            <Image style={styles.monthIcon} source={require('./../../assets/images/right.png')} resizeMode="contain" />
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        }
                        ItemSeparatorComponent={this.renderSeparator}
                        ListEmptyComponent={this.listEmpty()}
                        ListHeaderComponent={this.header()}
                    />
                </View>
            </View>
        )

    }
    //handling onPress action  
    getListViewItem = (item) => {
        var message = this.props.activeTab == "0"? "Your Consuption is "+ item.value +"kWh as on "+moment(item.date).format("DD/MM YYYY"):"Your Consuption price is "+ item.value +"kr as on "+moment(item.date).format("DD/MM YYYY") ;
        Alert.alert("Berry",  message);
    }

    loader() {
        if (this.props.isLoading) {
            return (
                <View style={styles.horizontal}>
                    <ActivityIndicator size="large" color="#FF4B9B" />
                </View>
            )
        }
    }
    render() {
        return (
            <Fragment>
                <StatusBar barStyle="light-content" backgroundColor="#2C2F36" />
                <SafeAreaView style={styles.statusBar} />
                <SafeAreaView style={styles.container}>
                    {this.loader()}
                    <View style={styles.wrapper}>
                        {this.section1()}
                        {this.section2()}
                    </View>
                </SafeAreaView>
            </Fragment>

        );
    }
}

const styles = StyleSheet.create({
    container: { flex: 9, backgroundColor: "#2C2F36", width },
    wrapper: {
        flex: 9,
        width,
    },
    section1: {
        borderWidth: 0,
        flex: 2.5,
        elevation: 2,
        shadowColor: '#2C2F36',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2
    },
    monthPicker: { 
        borderWidth: 0,
         flexDirection: "row",
          justifyContent: "space-between", padding: 10, },
    section2: {
        borderWidth: 0,
        flex: 6.5
    },
    arrows: { 
        padding: 8,
         borderWidth: 0 },
    monthBox: { 
        justifyContent: "center",
         alignContent: "center",
          alignItems: "center" },
    tabStyle: { 
        padding: 10,
         justifyContent: "center",
          alignContent: "center",
           flexDirection: "row", 
           borderWidth: 0 },
    tabBlock: { 
        backgroundColor: "#32363D", 
        borderRadius: 25,
         flexDirection: "row", },
    tabButton: { 
        borderRadius: 25, 
        paddingTop: 8,
         paddingBottom: 8,
          width: 137 
        },
    tabText: { 
        fontWeight: "600", 
    fontSize: 12, 
    lineHeight: 16,
     textAlign: "center",
      color: "#fff", 
      fontStyle: "normal"
     },
    flatListHeader: { 
        padding: 5, 
        paddingBottom: 15, 
        paddingTop: 15,
         justifyContent: "center",
          alignContent: "center",
           borderWidth: 0
         },
    textType: {
        color: "#FFFFFF",
        fontSize: 16,
        textAlign: "center",
        fontStyle: "normal",
        fontWeight: "bold",
        opacity: 0.6,
    },
    fWhValue: {

        fontSize: 32,
        textAlign: "center",
        fontStyle: "normal",
        fontWeight: "bold", lineHeight: 38
    },
    statusBar: {
        backgroundColor: "#2C2F36",
        color: "white"
    },
    scrollView: {

    },
    headerBox: {
        padding: 10,
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center"
    },
    headerText: {
        color: "#fff",
        fontSize: 18,
        textAlign: "center",
        fontStyle: "normal",
        fontWeight: "600",
        lineHeight: 21
    },
    monthIcon: {
        height: 15,
        width: 15,
        resizeMode: "contain"
    },
    horizontal: {
        height,
        borderWidth: 0,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 0,
        position: 'absolute',
        left: 0,
        right: 0,
        zIndex: 1

    },

    itemValue: {
        color: "#fff",
        fontSize: 14,
        lineHeight: 17,
        fontStyle: "normal"
    },
    itemSign: {
        color: "#fff",
        fontSize: 10,
        lineHeight: 12,
        fontStyle: "normal",
        textAlign: "center",
        textAlignVertical: "center",
        padding: 5,
    },
    itemBorder: {

        backgroundColor: "#fff",
        height: 1,
        padding: 0,
        opacity: 0.6
    },
    emptyBarView: { flex: 6, justifyContent: "center", alignItems: "center" },
    emptyText: {
        textAlign: "center",
        fontSize: 14,
        color: "#fff",
        lineHeight: 17,
        fontWeight: "600",
        fontStyle: "normal"
    },
    itemBox: {
        borderWidth: 0,
        flex: 2,
        paddingLeft: 8,
        paddingRight: 8,
        paddingTop: 17,
        paddingBottom: 17,
        flexDirection: "row",
        justifyContent: "space-between",
        alignContent: "center",
        alignItems: "center"
    },
    innerItemBox: { flexDirection: "row", },
    imageCenterBox: { justifyContent: "center", alignContent: "center" }


});