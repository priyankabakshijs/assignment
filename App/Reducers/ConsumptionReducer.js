import * as Actions from '../Actions/ActionTypes';

const ConsumptionReducer = (state = { isLoading: false, error: undefined, data: [], activeTab: 0, currentDate: new Date(), kWhTotal: 0, krTotal: 0, errorMessage: "No data found" }, action) => {
    switch (action.type) {
        case Actions.SERVICE_PENDING:
            return Object.assign({}, state, {
                isLoading: true,
            });
        case Actions.SERVICE_EMPTY:
            return Object.assign({}, state, {
                isLoading: false,
                errorMessage: state.errorMessage,
                data: []

            });
        case Actions.SERVICE_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                error: action.error,
                kWhTotal: 0,
                krTotal: 0

            });
        case Actions.SERVICE_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                data: action.data,
                kWhTotal: action.kWhTotal,
                krTotal: action.krTotal
            });
        case Actions.SET_DATE_TIME:
            return Object.assign({}, state, {
                activeTab: action.activeTab
            });
        case Actions.CHANGE_MONTH:
            return Object.assign({}, state, {
                currentDate: action.currentDate
            });

        default:
            return state;
    }
}

export default ConsumptionReducer;