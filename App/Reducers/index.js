import { combineReducers, createStore ,applyMiddleware,compose} from 'redux';
import thunk from 'redux-thunk';
import ConsumptionReducer from './ConsumptionReducer'

const AppReducers = combineReducers({
    ConsumptionReducer,
});

const rootReducer = (state, action) => {
	return AppReducers(state,action);
}

let store = createStore(rootReducer, compose(applyMiddleware(thunk)));

export default store;