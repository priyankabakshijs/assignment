***************************************************************************
*                             Explaination                                * 
***************************************************************************
*                                                                           
* React-native used 0.61.5                                                  
*                                                                           
* *************************************************************************
* Following react-native packages we used.                                *
* *************************************************************************
*
* react-redux 7.1.3
* redux       4.0.5
* redux-thunk 2.3.0
* moment      2.24.0
*
****************************************************************************
*                       How to Run in Android/iOS                          *
****************************************************************************
*  Goto you Project Directory in terminal -  cd assignment
*  run command for npm install
*  run the command to start package, make sure you started the emulator
*                       
*                       react-native start
*                     react-native run-android
*                       react-native run-ios  
*  
*  The project will be opened in the emulator.
*
****************************************************************************
*                             How to Work                                  * 
****************************************************************************
*  Initally load current Month of Cunsumption Data.
*  On change tab, you can see kWh and kr of Cunsumption Data.  
*  Changed the month on Next/Prev arrow from Month Picker. 
*  on Next/Prev Arrow, cunsumption data will be loaded for month. 
*  "No data found" will be shown if data will not come. 
****************************************************************************
